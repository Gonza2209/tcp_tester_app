﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using ModBusTcp;

namespace TCP_MODBUS_TESTER
{
    public partial class Form1 : Form
    {
        private ProtocoloModBusTcp my_connexion;
        private Random rand;
        private int conn_ok;
        private bool stop = false;
        public Form1()
        {
            InitializeComponent();

            my_connexion = new ProtocoloModBusTcp();

            rand = new Random();

            string myip = ip_TextBox.Text;
            conn_ok = 0;
        }

        private void start_button_Click(object sender, EventArgs e)
        {
            stop = false;
            read_textBox.Text = "";
            timer1.Interval = (int) lapse_UpDown.Value;
            timer1.Start();
        }

        private void stop_button_Click(object sender, EventArgs e)
        {
            stop = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            int bytes = (int) bytes_UpDown.Value;
            ushort words = (ushort) (bytes/4);

            byte[] read_val = new byte[bytes];

            if (my_connexion == default(ProtocoloModBusTcp))
                return;

            ushort port;

            if (ushort.TryParse(port_textBox.Text, out port) == false)
                return;

            timer1.Stop();

            if (stop == true)
                return;

            if (my_connexion.connected == false)
            {
                try 
                {
                    my_connexion.connect(ip_TextBox.Text, port, true);
                }
                catch (Exception error) 
                {
                    read_textBox.Text = error.ToString();
                }
                

                if (my_connexion.connected == false) 
                {
                    read_textBox.Text += "ERROR CONEXION";
                    return;
                }

                if (enable_checkBox.Checked == true)
                    my_connexion.ReadHoldingRegister((ushort)rand.Next(ushort.MaxValue), 1, 1, words, ref read_val);
                else
                    Thread.Sleep(100);

                my_connexion.disconnect();

                if (read_val == null)
                {
                    read_textBox.Text = "ERROR LECTURA";
                    return;
                }

                conn_ok++;
                ok_textBox.Text = conn_ok.ToString();

                if (enable_checkBox.Checked == true)
                    read_textBox.Text = BitConverter.ToString(read_val);
            }

            timer1.Start();
        }

        private void lapse_UpDown_ValueChanged(object sender, EventArgs e)
        {
            timer1.Interval = (int) lapse_UpDown.Value;
        }


    }
}
