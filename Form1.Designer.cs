﻿namespace TCP_MODBUS_TESTER
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.port_textBox = new System.Windows.Forms.TextBox();
            this.ip_TextBox = new System.Windows.Forms.MaskedTextBox();
            this.start_button = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.ok_textBox = new System.Windows.Forms.TextBox();
            this.read_textBox = new System.Windows.Forms.TextBox();
            this.lapse_UpDown = new System.Windows.Forms.NumericUpDown();
            this.bytes_UpDown = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.enable_checkBox = new System.Windows.Forms.CheckBox();
            this.stop_button = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.lapse_UpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bytes_UpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // port_textBox
            // 
            this.port_textBox.Location = new System.Drawing.Point(33, 84);
            this.port_textBox.Name = "port_textBox";
            this.port_textBox.Size = new System.Drawing.Size(102, 20);
            this.port_textBox.TabIndex = 1;
            this.port_textBox.Text = "502";
            this.port_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // ip_TextBox
            // 
            this.ip_TextBox.Location = new System.Drawing.Point(33, 36);
            this.ip_TextBox.Mask = "###.###.###.###";
            this.ip_TextBox.Name = "ip_TextBox";
            this.ip_TextBox.Size = new System.Drawing.Size(103, 20);
            this.ip_TextBox.TabIndex = 2;
            this.ip_TextBox.Text = "192168000100";
            this.ip_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // start_button
            // 
            this.start_button.Location = new System.Drawing.Point(188, 36);
            this.start_button.Name = "start_button";
            this.start_button.Size = new System.Drawing.Size(74, 27);
            this.start_button.TabIndex = 3;
            this.start_button.Text = "start";
            this.start_button.UseVisualStyleBackColor = true;
            this.start_button.Click += new System.EventHandler(this.start_button_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // ok_textBox
            // 
            this.ok_textBox.Location = new System.Drawing.Point(32, 178);
            this.ok_textBox.Name = "ok_textBox";
            this.ok_textBox.Size = new System.Drawing.Size(77, 20);
            this.ok_textBox.TabIndex = 4;
            this.ok_textBox.Text = "0";
            this.ok_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // read_textBox
            // 
            this.read_textBox.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.read_textBox.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.read_textBox.Location = new System.Drawing.Point(31, 272);
            this.read_textBox.Multiline = true;
            this.read_textBox.Name = "read_textBox";
            this.read_textBox.ReadOnly = true;
            this.read_textBox.Size = new System.Drawing.Size(252, 128);
            this.read_textBox.TabIndex = 6;
            // 
            // lapse_UpDown
            // 
            this.lapse_UpDown.Location = new System.Drawing.Point(188, 178);
            this.lapse_UpDown.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.lapse_UpDown.Name = "lapse_UpDown";
            this.lapse_UpDown.Size = new System.Drawing.Size(61, 20);
            this.lapse_UpDown.TabIndex = 7;
            this.lapse_UpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.lapse_UpDown.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.lapse_UpDown.ValueChanged += new System.EventHandler(this.lapse_UpDown_ValueChanged);
            // 
            // bytes_UpDown
            // 
            this.bytes_UpDown.Increment = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.bytes_UpDown.Location = new System.Drawing.Point(34, 130);
            this.bytes_UpDown.Minimum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.bytes_UpDown.Name = "bytes_UpDown";
            this.bytes_UpDown.Size = new System.Drawing.Size(75, 20);
            this.bytes_UpDown.TabIndex = 8;
            this.bytes_UpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.bytes_UpDown.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(186, 149);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 26);
            this.label1.TabIndex = 9;
            this.label1.Text = "connection \r\nInterval (mseg)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 116);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "length to read";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(32, 162);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(118, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "successful connections";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(30, 256);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "read information";
            // 
            // enable_checkBox
            // 
            this.enable_checkBox.AutoSize = true;
            this.enable_checkBox.Checked = true;
            this.enable_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.enable_checkBox.Location = new System.Drawing.Point(32, 218);
            this.enable_checkBox.Name = "enable_checkBox";
            this.enable_checkBox.Size = new System.Drawing.Size(138, 17);
            this.enable_checkBox.TabIndex = 13;
            this.enable_checkBox.Text = "ModBus reading enable";
            this.enable_checkBox.UseVisualStyleBackColor = true;
            // 
            // stop_button
            // 
            this.stop_button.Location = new System.Drawing.Point(188, 84);
            this.stop_button.Name = "stop_button";
            this.stop_button.Size = new System.Drawing.Size(74, 29);
            this.stop_button.TabIndex = 14;
            this.stop_button.Text = "sto p";
            this.stop_button.UseVisualStyleBackColor = true;
            this.stop_button.Click += new System.EventHandler(this.stop_button_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(32, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "IP address";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(32, 68);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(26, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Port";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(309, 412);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.stop_button);
            this.Controls.Add(this.enable_checkBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bytes_UpDown);
            this.Controls.Add(this.lapse_UpDown);
            this.Controls.Add(this.read_textBox);
            this.Controls.Add(this.ok_textBox);
            this.Controls.Add(this.start_button);
            this.Controls.Add(this.ip_TextBox);
            this.Controls.Add(this.port_textBox);
            this.Name = "Form1";
            this.Text = "TCP_Connection_Tester";
            ((System.ComponentModel.ISupportInitialize)(this.lapse_UpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bytes_UpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox port_textBox;
        private System.Windows.Forms.MaskedTextBox ip_TextBox;
        private System.Windows.Forms.Button start_button;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TextBox ok_textBox;
        private System.Windows.Forms.TextBox read_textBox;
        private System.Windows.Forms.NumericUpDown lapse_UpDown;
        private System.Windows.Forms.NumericUpDown bytes_UpDown;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox enable_checkBox;
        private System.Windows.Forms.Button stop_button;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}

